package sk.foundation.decibels;

import java.util.Date;

import org.codeandmagic.android.gauge.GaugeView;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.jjoe64.graphview.BarGraphView;
import com.jjoe64.graphview.GraphView.GraphViewData;
import com.jjoe64.graphview.GraphViewSeries;

public class DecibelsActivity extends Activity {

	Boolean mMode = false; // false -> fast , true -> slow
	Boolean mCalib = false;
	Boolean mLog = false;
	Boolean mMax = false;

	static final int MY_MSG = 1;
	static final int MAXOVER_MSG = 2;
	static final int ERROR_MSG = -1;

	static int PREFERENCES_GROUP_ID = 0;
	static final int RESET_OPTION = 1;
	static final int ABOUT_OPTION = 2;
	static final int FEEDBACK_OPTION = 4;
	static final int EXIT_OPTION = 3;

	SplEngine mEngine = null;
	Context mContext = DecibelsActivity.this;

	GaugeView mGaugeView1;
	GaugeView mGaugeView2;

	BarGraphView mGraphView;
	GraphViewSeries mGraphSeries;
	long seriesCounter;
	
	ViewSwitcher mGaugeSwitcher;
	boolean showNext = true;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		mGaugeView1 = (GaugeView) findViewById(R.id.gauge_view1);
		mGaugeView2 = (GaugeView) findViewById(R.id.gauge_view2);
		
		mGraphView = new BarGraphView(this, "");

		GraphViewData[] initialZeroData = new GraphViewData[80];
		for (int i = 0; i < 80; i++) {
			initialZeroData[i] = new GraphViewData(i, 0);
			seriesCounter++;
		}

		mGraphSeries = new GraphViewSeries(initialZeroData);
		mGraphView.addSeries(mGraphSeries);

		mGraphView.setViewPort(1, 80);
		mGraphView.setScalable(true);
		mGraphView.getGraphViewStyle().setNumVerticalLabels(14);
		mGraphView.getGraphViewStyle().setVerticalLabelsWidth(100);
		mGraphView.setShowHorizontalLabels(false);
		mGraphView.setManualYAxisBounds(130, 0);
		LinearLayout layout = (LinearLayout) findViewById(R.id.graph);
		layout.addView(mGraphView);
		
		mGaugeSwitcher = (ViewSwitcher) findViewById(R.id.gauge_switcher);
		
		final ImageButton showGraphButton = (ImageButton) findViewById(R.id.btn_show_graphs);
		showGraphButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (showNext) {
					mGaugeSwitcher.showNext();
					showGraphButton.setImageResource(R.drawable.gauge);
				} else {
					mGaugeSwitcher.showPrevious();
					showGraphButton.setImageResource(R.drawable.graph);
				}
				
				showNext = !showNext;
			}
		});
		
		TextView foundationLink = (TextView) findViewById(R.id.foundation_link);
		foundationLink.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.foundation.sk"));
				startActivity(i);				
			}
		});
	}

	@Override
	public void onResume() {
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		super.onResume();
		start_meter();
	}

	@Override
	protected void onPause() {
		getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		stop_meter();
		super.onPause();
	}

	@Override
	public void onStop() {
		getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		stop_meter();
		super.onStop();
	}

	/**
	 * Handles start/stop Logging
	 * 
	 * @param set
	 */
	public void handle_log(boolean set) {
		if (set) {
			mEngine.startLogging();
		} else {
			mEngine.stopLogging();
			Date today = new Date();
			Toast.makeText(
					mContext,
					"Log saved to /sdcard/splmeter_" + today.getDate() + "_"
							+ today.getMonth() + "_" + (today.getYear() + 1900)
							+ ".xls", Toast.LENGTH_SHORT).show();
		}
	}

	/**
	 * Displays the maximum SPL Value
	 */
	public void display_max() {
		mMax = true;
		handle_mode_display();
		mEngine.showMaxValue();
	}

	/**
	 * Display appropriate mode in the mode Text View
	 */
	private void handle_mode_display() {
		if (mMode) {
		} else {
		}

		if (mCalib) {
		} else {
		}

		if (mLog) {
		} else {
		}

		if (mMax) {
		}
	}

	/**
	 * Sets the SPL Meter Mode
	 */
	public void setMeterMode(String mode) {
		mEngine.setMode(mode);
	}

	/**
	 * Starts the SPL Meter
	 */
	public void start_meter() {
		mCalib = false;
		mMax = false;
		mLog = false;
		mMode = false;
		mEngine = new SplEngine(mhandle, mContext);
		mEngine.start_engine();
	}

	/**
	 * Stops the SPL Meter
	 */
	public void stop_meter() {
		if (mEngine != null) {
			mEngine.stop_engine();
		}
		
		mEngine = null;
	}

	/**
	 * Handler for displaying messages
	 */
	public Handler mhandle = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case MY_MSG:
				float value = ((Double) msg.obj).floatValue();
				value = Math.min(130, value);
				mGaugeView1.setTargetValue(value);
				mGaugeView2.setTargetValue(value);
				mGraphSeries.appendData(new GraphViewData(seriesCounter++,
						value), true, 1000);
				// mSplDataTV.setText(" " + msg.obj);
				break;
			case MAXOVER_MSG:
				mMax = false;
				handle_mode_display();
				// mSplMaxButton.setTextColor(Color.parseColor("#6D7B8D"));
				break;
			case ERROR_MSG:
				Toast.makeText(mContext, "Error " + msg.obj, Toast.LENGTH_LONG)
						.show();
				// stop_meter();
				break;
			default:
				super.handleMessage(msg);
				break;
			}
		}

	};

	/**
	 * Reset the SPL Meter
	 */
	public void reset_meter() {
		AlertDialog alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setTitle("Reset SPL Meter");
		alertDialog.setMessage("Do you want to reset the SPL Meter?");
		alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				mEngine.stop_engine();
				mEngine.reset();
				mEngine = new SplEngine(mhandle, mContext);
				mEngine.start_engine();
				Toast.makeText(mContext, "Calibration reset",
						Toast.LENGTH_SHORT).show();
				return;
			}
		});

		alertDialog.setButton2("Cancel", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				return;
			}
		});
		alertDialog.show();
	}
}